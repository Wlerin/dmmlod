import re
import datetime
import itertools
import binascii

import pytz
import asyncio
import aiohttp
import aiofiles
from Crypto.Cipher import AES
import m3u8


from .defaults import *

TOKYO_TZ = pytz.timezone('Asia/Tokyo')
video_chunk_re = re.compile(r'media_b\d+_(\d+).ts')


def format_date(dt=None, long_date=False):
    if not dt:
        dt = datetime.datetime.now(tz=pytz.timezone('Asia/Tokyo'))

    if long_date:
        return '{:04d}-{:02d}-{:02d}_{:02d}:{:02d}'.format(dt.year, dt.month, dt.day, dt.hour, round(dt.minute/30)*30)
    else:
        return '{:02d}{:02d}{:02d}{:02d}{:02d}'.format(dt.year%100, dt.month, dt.day, dt.hour, round(dt.minute/30)*30)


class DMMClient:
    def __init__(self, config=None, event_loop=None):
        if not config or not isinstance(config, dict):
            self.config = {}
        else:
            self.config = config

        # TODO: create session
        if not event_loop:
            self.loop = asyncio.get_event_loop()
        else:
            self.loop = event_loop

    # TODO: accept additional arguments added recently?
    async def save_playlist(self, playlist, savedir=None, filename=None, skip_exists=False, key=None):
        if playlist in self.config.get('playlists', {}):
            playlist_url = self.config['playlists'][playlist]
        else:
            # TODO: check if valid url
            playlist_url = playlist

        if not filename:
            group = self.config.get('group')
            label = playlist if playlist != playlist_url else 'lod'
            date = format_date()
            filename = '_'.join((e for e in (group, date, label) if e))

        savedir = os.path.join((savedir or DEFAULT_SAVE_DIRECTORY), filename)
        os.makedirs(savedir, exist_ok=True)

        base_uri = playlist.rsplit('/', 1)[0] + '/'

        async with aiohttp.ClientSession(headers=self.config.get('headers', DEFAULT_HEADERS)) as session:
            async with session.get(playlist_url, headers=self.config.get('headers', DEFAULT_HEADERS)) as r:
                # TODO: handle 404 responses
                if r.status == 404:
                    pass

                # TODO: handle other errors
                r.raise_for_status()

                playlist_obj = m3u8.loads(await r.text())

            playlist_obj.base_uri = base_uri
            # get best chunklist
            best_variant = sorted(playlist_obj.playlists, key=lambda x: x.stream_info.bandwidth)[-1]
            chunklist_url = best_variant.absolute_uri

            async with session.get(chunklist_url, headers=self.config.get('headers', DEFAULT_HEADERS)) as r:
                # TODO: handle 404 responses
                if r.status == 404:
                    pass

                # TODO: handle other errors
                r.raise_for_status()

                chunklist_obj = m3u8.loads(await r.text())
                # TODO: check response headers for cookies etc.

        chunklist_obj.base_uri = base_uri

        keyfile = '{}.key'.format(filename)
        if not key:
            keyuri = chunklist_obj.keys[0].uri
            # TODO: don't bother with saving m3u8 + keyfile?
            decryption_key = await self.save_decryption_key(keyuri, savedir, keyfile)
        else:
            decryption_key = key

        if chunklist_obj.is_endlist:
             final_obj = await self.save_seekable_playlist(chunklist_obj,
                                                           savedir,
                                                           key=decryption_key,
                                                           skip_exists=skip_exists)
        else:
            # TODO: catch errors
            final_obj = await self.save_live_playlist(chunklist_url, chunklist_obj, savedir, key=decryption_key)
            final_obj.is_endlist = True

        # have to deal with the key and merging manually
        playlist_file_path = '{}/{}.m3u8'.format(savedir, filename)

        if not decryption_key:
            final_obj.keys[0].uri = keyfile

        # TODO: make this asynchronous
        text = final_obj.dumps()
        await save_m3u8(text, playlist_file_path, strip_keys=bool(decryption_key))

        return playlist_file_path

    async def save_decryption_key(self, url, savedir, keyfile):
        fullpath = os.path.join(savedir, keyfile)
        licenseUID = self.config.get('licenseUID')
        if not licenseUID:
            return None

        async with aiohttp.ClientSession(headers=self.config.get('headers', DEFAULT_HEADERS),
                                         cookies={"licenseUID": licenseUID}) as session:
            async with session.get(url) as r:
                if r.status == 200:
                    key = await r.read()
                    if len(key) > 32:
                        print('Fetching key probably failed')
                        return None
                    else:
                        with open(fullpath, 'wb') as outfp:
                            outfp.write(key)
                        return key
                else:
                    print('Failed to retrieve key from {}'.format(url))

    async def save_seekable_playlist(self, chunklist_obj, savedir, key, skip_exists):
        segment_urls = [e.absolute_uri for e in chunklist_obj.segments]
        segment_paths = [os.path.join(savedir, extract_filename(e)) for e in segment_urls]
        if skip_exists:
            segment_paths = [e for e in segment_paths if not os.path.exists(e)]

        args = zip(segment_urls, segment_paths)
        sem = asyncio.Semaphore(self.config.get('worker_count', DEFAULT_WORKER_COUNT))
        tasks = []

        timeout = aiohttp.ClientTimeout(total=None,
                                        sock_connect=self.config.get('timeout', TIMEOUT),
                                        sock_read=self.config.get('timeout', TIMEOUT))
        local_session = aiohttp.ClientSession(headers=self.config.get('headers', DEFAULT_HEADERS), timeout=timeout)
        for item in args:
            task = asyncio.ensure_future(bound_call(sem, self.save_segment, *item,
                                                    session=local_session, key=key))
            tasks.append(task)

        responses = asyncio.gather(*tasks)
        await responses

        for segment in chunklist_obj.segments:
            segment.uri = extract_filename(segment.uri)

    async def save_segment(self, url, destfile, session, key=None, iv=None, failed_callback=None):
        chunksize = self.config.get('chunksize', DEFAULT_CHUNKSIZE)
        first_attempt = True
        exc = None
        status = None

        for attempt in range(1, self.config.get('max_attempts', MAX_ATTEMPTS)):
            if not first_attempt:
                asyncio.sleep(attempt, loop=self.loop)
            else:
                first_attempt = False

            # Does this need to be inside the try-except?
            r = await session.get(url)
            if r.status != 200:
                if r.status == 404:
                    print('Got 404 when fetching {}'.format(url))
                    status = r.status
                    exc = None
                continue

            if key:
                if not iv:
                    iv = hex(get_media_sequence(url))[2:]
                    iv = "0" * (32 - len(iv)) + iv
                    iv = binascii.unhexlify(iv)
                crypt_mode = AES.MODE_CBC
                decryptor = AES.new(key, crypt_mode, IV=iv)
                async with aiofiles.open(destfile, 'wb') as outfp:
                    try:
                        while True:
                            cryptchunk = await r.content.read(chunksize)
                            if not cryptchunk:
                                break
                            plainchunk = decryptor.decrypt(cryptchunk)
                            await outfp.write(plainchunk)
                    except (asyncio.TimeoutError, aiohttp.client_exceptions.ClientConnectionError) as e:
                        print(destfile, e)
                        exc = e
                        status = None
                        continue
                    else:
                        break
                    finally:
                        r.close()
            else:
                async with aiofiles.open(destfile, 'wb') as outfp:
                    try:
                        while True:
                            chunk = await r.content.read(chunksize)
                            if not chunk:
                                break
                            outfp.write(chunk)
                    except (asyncio.TimeoutError, aiohttp.client_exceptions.ClientConnectionError) as e:
                        print(destfile, e)
                        exc = e
                        status = None
                        continue
                    else:
                        break
                    finally:
                        r.close()
        else:
            os.remove(destfile)
            print('Attempt to save {} finally failed with {}'.format(destfile, exc or status))
            if failed_callback:
                failed_callback(url=url, filename=os.path.basename(destfile))

    async def save_live_playlist(self, chunklist_url, chunklist_obj, savedir, key=None):
        errors = 0
        failed = []
        tasks = []
        known_segments = set()
        base_uri = chunklist_url.rsplit('/', 1)[0] + '/'

        target_duration = 15  # chunklist_obj.target_duration

        def failed_callback(**kwargs):
            failed.append(kwargs.get('filename'))

        timeout = aiohttp.ClientTimeout(total=None,
                                        sock_connect=self.config.get('timeout', TIMEOUT),
                                        sock_read=self.config.get('timeout', TIMEOUT))
        async with aiohttp.ClientSession(headers=self.config.get('headers', DEFAULT_HEADERS),
                                         timeout=timeout,
                                         loop=self.loop) as local_session:
            # push the initial segments onto the queue
            for segment in chunklist_obj.segments:
                url = segment.absolute_uri
                filename = extract_filename(url)
                dest = os.path.join(savedir, filename)
                print('Saving {}'.format(filename))
                task = asyncio.ensure_future(self.save_segment(url, dest, local_session, key,
                                                               failed_callback=failed_callback), loop=self.loop)
                tasks.append(task)
                segment.uri = filename
                known_segments.add(filename)
            asyncio.sleep(target_duration, loop=self.loop)

            # continue fetching the chunklist until it errors out
            while errors < MAX_404_ERRORS:
                async with local_session.get(chunklist_url) as r:
                    if r.status != 200:
                        errors += 1
                        print('Caught {} while fetching chunklist'.format(r.status))
                        asyncio.sleep(min(errors, target_duration))
                        continue
                    partlist = m3u8.loads(r.text())
                    partlist.base_uri = base_uri
                    for segment in partlist.segments:
                        filename = extract_filename(segment.uri)
                        if filename in known_segments:
                            continue
                        url = segment.absolute_uri
                        segment.key = None  # clear keys on new segments
                        dest = os.path.join(savedir, filename)
                        print('Saving {}'.format(filename))
                        task = asyncio.ensure_future(self.save_segment(url, dest, local_session, key,
                                                                       failed_callback=failed_callback), loop=self.loop)
                        tasks.append(task)
                        segment.uri = filename
                        chunklist_obj.add_segment(segment)
                        known_segments.add(filename)
                    asyncio.sleep(target_duration)

            responses = asyncio.gather(*tasks)
            await responses

        if failed:
            print('Errors:', *failed, sep='\n')

        segments = len(chunklist_obj.segments)
        i = 0
        while i < segments:
            if chunklist_obj.segments[i].uri in failed:
                chunklist_obj.segments.pop(i)
                segments -= 1
            else:
                i += 1

        return chunklist_obj


def get_media_sequence(file):
    return int(video_chunk_re.search(file).group(1))


def extract_filename(url):
    return url.rsplit('/', 1)[-1].split('?')[0]


async def bound_call(sem, func, *args, **kwargs):
    async with sem:
        await func(*args, **kwargs)


async def save_m3u8(body, dest, strip_keys=False):
    async with aiofiles.open(dest, 'w', encoding='utf8') as outfp:
        if strip_keys:
            for line in body.split('\n'):
                if 'EXT-X-KEY' in line:
                    continue
                await outfp.write(line)
        else:
            await outfp.write(body)
