# subcommands
# setup: ask for basic information, generate config files
# set: set and save various config options, need to think about how to do this
# save: save video segments from dmm, decrypting if possible
# decrypt: decrypt saved video segments
# merge: combine decrypted video segments using ffmpeg
# - without ffmpeg simple concatenation corrupts some streams

# TODO: more advanced api usage
# e.g. login, getting playlists from html5 endpoint, etc.
import argparse
import os
import shutil
import yaml
import base64

from . import config
from .client import DMMClient


def do_save(args):
    try:
        data = config.load(args.config_file)
    except FileNotFoundError:
        print('For best results, generate a config file using `dmmlod setup` first.')
        data = {}

    if args.keyfile:
        with open(args.keyfile, 'rb') as infp:
            key = infp.read()
    elif args.key:
        key = base64.b64decode(args.key)
    else:
        key = None

    client = DMMClient(config=data)

    if args.license:
        client.config['licenseUID'] = args.license

    client.loop.run_until_complete(
        client.save_playlist(args.playlist, args.savedir, args.name,
                             skip_exists=args.skip_exists, key=key)
    )
    if args.merge:
        # TODO: merge
        raise NotImplementedError


def do_decrypt(args):
    raise NotImplementedError


def do_merge(args):
    raise NotImplementedError


def do_set(args):
    data = config.load(args.config_file)
    data.update(args.options)
    if args.license:
        data['licenseUID'] = args.license

    config.save(data, args.config_file)


def do_setup(args):
    # ask for paths/urls/etc.
    # save config
    srcpath = args.config_file  # will search paths anyway if None
    if args.use_standard_config_path:
        savepath = config.find_config()
    else:
        savepath = srcpath or config.find_config()

    b_override = args.override

    data = config.load(srcpath, ignore_errors=True)
    if data:
        print('Loaded config from {}'.format(srcpath))

    if args.options:
        data.update(args.options)

    print('DMM LOD Archival Tool needs some information about your system and what you will be recording:')
    if b_override or not data.get('save_directory'):
        default_path = data.get('save_directory') or args.savedir
        choice = input('Where do you want to save downloaded videos? [{}]: '.format(default_path))
        if not choice:
            data['save_directory'] = os.path.expanduser(default_path)
        elif choice.startswith('~'):
            print('Converting user path to absolute...')
            data['save_directory'] = os.path.expanduser(choice)
        elif choice.startswith('/') or choice[1] == ':':
            print('Using absolute path...')
            data['save_directory'] = choice
        else:
            print('Converting relative path to absolute...')
            data['save_directory'] = os.path.abspath(choice)

    if b_override or not data.get('group'):
        choice = input('Do you wish to record for one specific group? [y/N]: ')
        if choice.lower().startswith('y'):
            default_group = data.get('group') or 'akb48'
            choice = input('Which group will you be recording? [{}]: '.format(default_group))
            if not choice:
                data['group'] = default_group
            else:
                data['group'] = choice

    # TODO: more robust cookie management
    if b_override or not data.get('licenseUID'):
        if args.license:
            license = args.license
        else:
            print('Please obtain your current licenseUID from a browser and enter it here: ')
            license = input()
        # TODO: check that it's the right length etc.
        data['licenseUID'] = license

    # TODO: custom filename patterns for each playlist
    if b_override or not data.setdefault('playlists', {}):
        # The default behaviour assumes one is only trying to record for one group per config file
        print("The playlist urls for live performances seem to be fairly constant. You can store them\n"
              "in the config file to access by name instead of pasting the whole url.")
        choice = input('Do you wish to do this now? [y/N]: ').lower()
        while choice.startswith('y'):
            choice = input('Enter a convenient name for the playlist (e.g. livehd): ')
            name = choice
            if name in data['playlists']:
                print('This name already exists, with the following URL:\n', data['playlists'][name])
                choice = input('Do you wish to overwrite? [y/N]: ').lower()
                if not choice.startswith('y'):
                    continue
            print('Enter the full playlist URL: ')
            # TODO: validate
            url = input()
            data['playlists'][name] = url
            print('{}: {}'.format(name, url))
            choice = input('Do you wish to add another playlist? [y/N]: ').lower()

    # TODO: other necessary headers?
    if b_override or not data.get('headers', {}).get('User-Agent'):
        print('No User-Agent header found. Please copy and paste the user-agent header used by your browser: ')
        ua_str = input()
        if ua_str:
            data.setdefault('headers', {})['User-Agent'] = ua_str
        else:
            # Client supplies the default if none is in config
            print('No User-Agent was provided, the default will be used.')

    # look for ffmpeg
    if b_override or not data.get('ffmpeg'):
        ffmpeg_path = shutil.which('ffmpeg')
        if not ffmpeg_path:
            print('Cannot locate FFmpeg. Please provide the path to the ffmpeg executable.')
            data['ffmpeg'] = input()
        else:
            data['ffmpeg'] = 'ffmpeg'  # on PATH

    # check that ffmpeg is runnable
    while not shutil.which(data.get('ffmpeg')):
        print('Cannot locate or execute ffmpeg, please supply the correct path: ')
        data['ffmpeg'] = input()

    print('Creating directories...')
    os.makedirs(data['save_directory'], exist_ok=True)
    os.makedirs(os.path.dirname(savepath), exist_ok=True)

    print('Saving config to {}...'.format(savepath))
    # TODO: allow saving to a different location?
    config.save(data, savepath)
    # TODO: try linking config to download directory


def do_get(args):
    data = config.load(args.config_file)
    for field in args.fields:
        print('{}:'.format(field))
        if isinstance(data[field], dict):
            if args.show_full:
                for key, val in data[field].items():
                    print('{}: {}'.format(key, val))
            else:
                print(*sorted(data[field].keys()), sep='\n')
        elif isinstance(data[field], list):
            print(*sorted(data[field]), sep='\n')
        else:
            print(data[field])
        print()


class ConfigOptionParser(argparse.Action):
    def __call__(self, parser, namespace, values, option_string=None):
        result = {}
        for option in values:
            key, val = option.split('=', 1)
            val = val.strip()

            # TODO: better support nested options, e.g. playlists, headers
            try:
                data = yaml.safe_load(val)
            except yaml.YAMLError as e:
                data = val
            result[key] = data
        setattr(namespace, 'options', result)


def build_parser():
    parser = argparse.ArgumentParser(prog="DMM-LOD-Archival-Tool",
                                     description="Download and process live streams from DMM.com")
    parser.add_argument('--license', '-L', help='licenseUID cookie')
    parser.add_argument('--config-file', '-C', help='Config file to load from or save to')
    parser.add_argument('--key', '-k', help='Base64 encoded decryption key')
    parser.add_argument('--keyfile', '-K', help='Path to decoded decryption key file')
    parser.add_argument("--savedir", "-o", help="Output directory for saved files")

    sp = parser.add_subparsers()

    save_parser = sp.add_parser('save', help='Save files from a DMM playlist (live or recorded)')
    save_parser.add_argument("playlist", help="Either the URL of the playlist.m3u8, "
                                              "or a playlist name to load from config, like \"livehd\". "
                                              "Automatically downloads the best available stream. "
                                              "Note that fixed camera and hd use different playlist "
                                              "urls from the default sd stream")
    save_parser.add_argument("--name", "-n", help="Stream name, used as a folder name. If not provided "
                                                  "Will be generated from group, date, and playlist.")
    save_parser.add_argument("--skip-exists", "-x", action="store_true",
                             help="Skip existing files (use to patch failed downloads of VODs)")
    # -D for decrypt is redundant, since save will do that automatically if possible, and if it isn't there's
    # no point in having such an option.
    save_parser.add_argument("--merge", "-M", action="store_true",
                             help="Merge on completion. Only possible with either a valid license or key/keyfile. "
                                  "Skipped if there are errors during the download.")
    save_parser.set_defaults(func=do_save)

    decrypt_parser = sp.add_parser('decrypt', help='Decrypt files saved from DMM.')
    decrypt_parser.add_argument('source-directory', help='Directory holding the saved, encrypted TS files')
    decrypt_parser.add_argument('dest-directory', help='Directory to save the decrypted files to.')
    decrypt_parser.set_defaults(func=do_decrypt)

    merge_parser = sp.add_parser('merge', help='Decrypt and concatenate a saved stream')
    # lod.py merge source_directory destination_file -k key
    merge_parser.add_argument('source-directory', help='Directory holding the saved, encrypted TS files')
    merge_parser.add_argument('dest-file', help='File for full decrypted stream')
    merge_parser.set_defaults(func=do_merge)

    set_parser = sp.add_parser('set', help='Set config options')
    set_parser.add_argument('options', nargs='*', action=ConfigOptionParser, help='Series of key=value pairs')
    set_parser.set_defaults(func=do_set)

    setup_parser = sp.add_parser('setup', help='Create folders and ask for necessary information.')
    setup_parser.add_argument('options', nargs='*', action=ConfigOptionParser, help='Series of key=value pairs')
    setup_parser.add_argument('--override', action='store_true', help='Ask for all config options, '
                                                                      'even if already present')
    setup_parser.add_argument('--use-standard-config-path', '-S', action='store_true',
                              help='Save to a standard path, even if a config file is given.')
    setup_parser.set_defaults(func=do_setup)

    get_parser = sp.add_parser('get', help='Display config options')
    get_parser.add_argument('--show-full', '-F', action='store_true', help='Display full keys and values for dict items '
                                                                      '(otherwise shows only keys)')
    get_parser.add_argument('fields', nargs='+', help="List of config fields to display")
    get_parser.set_defaults(func=do_get)

    # TODO: list available playlists
    return parser


def main():
    # this is what will be called by dmmlod once it is installed
    parser = build_parser()
    args = parser.parse_args()
    args.func(args)
