from setuptools import setup

deps = [
    "m3u8",
    "aiohttp",
    "pycryptodome",
    "pyyaml",
    "pytz",
    "appdirs",
    "cchardet",  # can this install without hassle on windows?
    "aiodns",
    "aiofiles",
    "pysocks"
]

setup(
    name='dmmlod',
    version='0.0.2dev',
    packages=['dmmlod',],
    license='MIT',
    long_description=open('README.md').read(),
    install_requires=deps,
    python_requires=">=3.5, <4",
    entry_points={
        "console_scripts": ["dmmlod=dmmlod.cli:main"]
    }
)